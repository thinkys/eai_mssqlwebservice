﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using PaymentMSSQLManager.Models;

namespace PaymentMSSQLManager.Services
{
    public class MssqlRepository
    {
        private const string CacheKey = "MssqlStore";

        public MssqlRepository()
        {
            
        }

        public int getQuota(int id)
        {
            int quota = 0;

            var dataTable = new DataTable();


            string connectionString = ConfigurationManager.ConnectionStrings["MssqlDatabase"].ConnectionString;

            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    string query = "Select * From T_User Where uid = @uid";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@uid", id);

                    cn.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            quota = (int)dr["quota"];
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return quota;
        }
        public string getUsername(int id)
        {
            var ctx = HttpContext.Current;
            string username = "";

            var dataTable = new DataTable();

 
               string connectionString = ConfigurationManager.ConnectionStrings["MssqlDatabase"].ConnectionString;

                    try
                    {
                        using (SqlConnection cn = new SqlConnection(connectionString))
                        {
                            string query = "Select * From T_User Where uid = @uid";
                            SqlCommand cmd = new SqlCommand(query, cn);
                            cmd.Parameters.AddWithValue("@uid", id);

                            cn.Open();

                            using (SqlDataReader dr = cmd.ExecuteReader())
                            {
                                if (dr.Read())
                                {
                                    username = (string)dr["username"];
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
            return username;
        }
        
        public int AddAmount(int uid, int amount)
        {
            int result = 0;
            string connectionString = ConfigurationManager.ConnectionStrings["MssqlDatabase"].ConnectionString;

            try
            {
                using(SqlConnection cn = new SqlConnection(connectionString))
                {
                    amount = this.getQuota(uid) + amount;
                    string query = "UPDATE T_User SET quota = @amount WHERE uid = @uid";
                    SqlCommand cmd = new SqlCommand(query, cn);
                    cmd.Parameters.AddWithValue("@amount", amount);
                    cmd.Parameters.AddWithValue("@uid", uid);

                    cn.Open();
                    result = cmd.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch(Exception e)
            {
                throw e;
            }

            return this.getQuota(uid);
        }
        
        public string[] GetAllMssqlRows()
        {
            var ctx = HttpContext.Current;

            if(ctx != null)
            {
                return (string[])ctx.Cache[CacheKey];
            }

            string[] empty = new string[1];
            empty[0] = "empty";
            return empty;
        }

        public bool SaveMssql(Mssql mssql)
        {
            var ctx = HttpContext.Current;

            if(ctx != null)
            {
                try
                {
                    var currentData = ((Mssql[])ctx.Cache[CacheKey]).ToList();
                    currentData.Add(mssql);
                    ctx.Cache[CacheKey] = currentData.ToArray();

                    return true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
        return false;
        }
    }
}