﻿using PaymentMSSQLManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PaymentMSSQLManager.Services;
using System.Web.Http;
using Newtonsoft.Json;

namespace PaymentMSSQLManager.Controllers
{
    public class MssqlController : Controller
    {
        private string Name;
        private MssqlRepository mssqlRepository;
        // GET: MSSQL
        public ActionResult Index()
        {
            return View();
        }

        public MssqlController()
        {
            
        }

        [System.Web.Http.Route("{uid:int}")]
        [System.Web.Http.HttpGet]
        public string GetById(int uid)
        {
            this.mssqlRepository = new MssqlRepository();
            string username = this.mssqlRepository.getUsername(uid);
            int quota = this.mssqlRepository.getQuota(uid);

            return username + ' ' + quota.ToString();       
        }

        [System.Web.Http.Route("{uid:int}/{amount:int")]
        [System.Web.Http.HttpGet]
        public string AddAmount(int uid, int amount)
        {
            this.mssqlRepository = new MssqlRepository();
            int result = this.mssqlRepository.AddAmount(uid, amount);
            return this.mssqlRepository.getUsername(uid) + " " + result.ToString();
        }


    }
}